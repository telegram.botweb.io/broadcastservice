from django.contrib import admin
from .models import Client, Message


# Register your models here.

@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    """Клиенты"""
    list_display = ("phone_number", 'id')


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    """Сообщения"""
    list_display = ("status_code", 'id')
