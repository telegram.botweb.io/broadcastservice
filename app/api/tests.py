from datetime import datetime, timedelta

from django.urls import include, path, reverse
from rest_framework import status
from rest_framework.test import APITestCase, URLPatternsTestCase
from .models import Client, Broadcast, Message
from django.utils import timezone


class ClientTests(APITestCase):

    def setUp(self):
        Client.objects.create(
            phone_number='79254082110',
            mobile_operator_code='925',
            tag='Максим',
            time_zone="Europe/Moscow"
        )

    def test_create_client(self):
        """Test Create Client by API"""
        Client.objects.last().delete()
        url = reverse('client-create')
        data = {
            "phone_number": "79254082110",
            "mobile_operator_code": "925",
            "tag": "Максим",
            "time_zone": "Europe/Moscow"
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(Client.objects.all()), 1)

    def test_client_update(self):
        """Test Update by PUT and PUTCH Client by API"""
        client = Client.objects.last()
        url = reverse('client-update', args=[client.id])
        data = {
            "phone_number": "7926408211",
            "mobile_operator_code": "926",
            "tag": "Максим",
            "time_zone": "Europe/Moscow"
        }
        response = self.client.put(url, data=data)

        client = Client.objects.get(id=client.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('phone_number'), '7926408211')
        self.assertEqual(client.phone_number, '7926408211')
        self.assertEqual(response.data.get('mobile_operator_code'), '926')
        self.assertEqual(client.mobile_operator_code, '926')

        data = {
            "tag": "Программист",
        }
        response = self.client.patch(url, data=data)

        client = Client.objects.get(id=client.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(client.phone_number, '7926408211')
        self.assertEqual(client.tag, 'Программист')

    def test_destroy_client(self):
        """Test Destroy Client by API"""
        client = Client.objects.last()
        url = reverse('client-destroy', args=[client.id])
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(len(Client.objects.all()), 0)


class BroadcastTests(APITestCase):

    def setUp(self):
        client1 = Client.objects.create(
            phone_number='79254082110',
            mobile_operator_code='925',
            tag='Максим',
            time_zone="Europe/Moscow"
        )
        client2 = Client.objects.create(
            phone_number='79254082111',
            mobile_operator_code='925',
            tag='Сергей',
            time_zone="Europe/Moscow"
        )
        client3 = Client.objects.create(
            phone_number='88005553535',
            mobile_operator_code='800',
            tag='Сергей',
            time_zone="US/Central"
        )
        broadcast = Broadcast.objects.create(
            start_time=datetime.now(),
            end_time=datetime.now() + timedelta(hours=5),
            text='Привет!',
            filter='800'
        )
        Message.objects.create(
            send_time=datetime.now(),
            status_code=0,
            broadcast=broadcast,
            client=client1
        )
        Message.objects.create(
            send_time=datetime.now(),
            status_code=0,
            broadcast=broadcast,
            client=client2
        )
        Message.objects.create(
            send_time=datetime.now(),
            status_code=1,
            broadcast=broadcast,
            client=client3
        )

    def test_broadcast_create(self):
        url = reverse('broadcast-create')

        now = timezone.now()
        end = (timezone.now() + timedelta(hours=10))
        data = {
            "start_time": now,
            "end_time": end,
            "text": "Привет!",
            "filter": ""
        }

        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Broadcast.objects.last().start_time.isoformat(), now.isoformat())
        self.assertEqual(Broadcast.objects.last().end_time.isoformat(), end.isoformat())

    def test_broadcast_update(self):
        broadcast = Broadcast.objects.last()
        url = reverse('broadcast-update', args=[broadcast.id])

        now = timezone.now()
        end = (timezone.now() + timedelta(hours=10))
        data = {
            "start_time": now,
            "end_time": end,
            "text": "Пока!",
            "filter": ""
        }

        response = self.client.put(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Broadcast.objects.last().start_time.isoformat(), now.isoformat())
        self.assertEqual(Broadcast.objects.last().text, 'Пока!')

        data = {
            "text": "Как дела?",
        }
        response = self.client.patch(url, data=data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Broadcast.objects.last().start_time.isoformat(), now.isoformat())
        self.assertEqual(Broadcast.objects.last().text, 'Как дела?')

    def test_broadcast_destroy(self):
        broadcast = Broadcast.objects.last()

        url = reverse('broadcast-destroy', args=[broadcast.id])
        response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(len(Broadcast.objects.all()), 0)

    def test_broadcast_statistic(self):
        url = reverse('broadcast-statistic')
        excepted_data = {'broadcast_count': 1, 'details': [{'ID': 1, 'Status Code 0': 2, 'Status Code 1': 1}]}
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, excepted_data)

    def test_broadcast_statistic_details(self):
        broadcast = Broadcast.objects.last()
        url = reverse('broadcast-statistic-detail', args=[broadcast.id])
        excepted_data = {'ID': 1, 'Status Code 0': 2, 'Status Code 1': 1}
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, excepted_data)
