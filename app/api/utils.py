from api.models import Client, Broadcast, Message


def get_all_clients():
    return Client.objects.all()


def get_all_broadcasts():
    return Broadcast.objects.all()


def get_broadcast_statistic():
    data = {}
    all_broadcast = Broadcast.objects.all()
    data['broadcast_count'] = all_broadcast.count()
    details = []
    for broadcast in all_broadcast:
        details.append({
            'id': broadcast.id,
            'status 0': Message.objects.filter(status_code=0, broadcast=broadcast).count(),
            'status 1': Message.objects.filter(status_code=1, broadcast=broadcast).count(),
        })
        # todo add status-codes
        data['details'] = details
    return data


def get_broadcast_statistic_by_pk(pk: int):
    broadcast = Broadcast.objects.filter(id=pk).last()
    if broadcast:
        data = {
            'id': broadcast.id,
            'status code 0': Message.objects.filter(status_code=0, broadcast=broadcast).count(),
            'status code 1': Message.objects.filter(status_code=1, broadcast=broadcast).count(),
        }
        # todo add status-codes
        return data
    else:
        return False
