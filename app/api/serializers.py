import logging

from rest_framework import serializers
from timezone_field.rest_framework import TimeZoneSerializerField
from api.models import Client, Broadcast

logger = logging.getLogger(__name__)


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ['id', 'phone_number', 'mobile_operator_code', 'tag', 'time_zone']

    time_zone = TimeZoneSerializerField()

    def create(self, validated_data):
        instance = super().create(validated_data)
        logger.info(f'Client {instance.id} was created with '
                    f'phone_number: {instance.phone_number}; '
                    f'mobile_operator_code: {instance.mobile_operator_code}; '
                    f'tag: {instance.tag}; '
                    f'time_zone: {instance.time_zone}')
        return instance

    def update(self, instance, validated_data):
        instance = super().update(instance, validated_data)
        logger.info(f'Client {instance.id} was changed data to '
                    f'phone_number: {instance.phone_number}; '
                    f'mobile_operator_code: {instance.mobile_operator_code}; '
                    f'tag: {instance.tag}; '
                    f'time_zone: {instance.time_zone}; ')
        return instance


class BroadcastSerializer(serializers.ModelSerializer):
    class Meta:
        model = Broadcast
        fields = '__all__'

    def create(self, validated_data):
        instance = super().create(validated_data)
        logger.info(f'Broadcast {instance.id} was created with '
                    f'start_time: {instance.start_time}; '
                    f'end_time: {instance.end_time}; '
                    f'text: {instance.text}; '
                    f'filter: {instance.filter}')
        return instance

    def update(self, instance, validated_data):
        instance = super().update(instance, validated_data)
        logger.info(f'Broadcast {instance.id} was changed data to'
                    f'start_time: {instance.start_time}; '
                    f'end_time: {instance.end_time}; '
                    f'text: {instance.text}; '
                    f'filter: {instance.filter}; ')
        return instance

