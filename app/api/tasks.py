import logging
import time
from datetime import datetime

import requests
from celery import shared_task
from django.utils import timezone
from itertools import filterfalse
from api.models import Broadcast, Client, Message

logger = logging.getLogger(__name__)

def get_sorted_clients(broadcast: Broadcast):
    if broadcast.filter:
        if broadcast.filter.isdigit():
            clients = Client.objects.filter(mobile_operator_code=broadcast.filter)
        else:
            clients = Client.objects.filter(tag=broadcast.filter)
    else:
        clients = Client.objects.all()

    sorted_clients = []
    timezones = sorted([datetime.now(i['time_zone']) for i in clients.values('time_zone').distinct()])
    sorted_tzinfo = [i.tzinfo for i in timezones]
    sorted_tzinfo.reverse()
    for tzinfo in sorted_tzinfo:
        sorted_clients += list(Client.objects.filter(time_zone=tzinfo))
    return sorted_clients


def send_message(msg_id: int, text: str, phone_number: str):
    base_url = 'https://probe.fbrq.cloud/v1/'
    token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2OTIxODg5NjgsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Ik9rdWxvdk1ha3NpbSJ9.qx7DLhtZqSMirt7jEr-IMePFGmNy8KCt7co-pKKgvm8'
    # todo token in venv

    headers = {'authorization': f'Bearer {token}'}

    req = requests.post(
        url=base_url + f'send/{msg_id}',
        headers=headers,
        json={
            'id': msg_id,
            'phone': int(phone_number),
            'text': text
        },
        timeout=5
    )
    logger.info(f'message_id: {msg_id} send_message response: {req.status_code} {req.text}')
    if req.status_code == 200:
        return req.json().get('code')
    else:
        return 1


@shared_task()
def start_broadcast(broadcast_id: int):
    broadcast = Broadcast.objects.filter(id=broadcast_id).last()
    if broadcast:
        sorted_clients = get_sorted_clients(broadcast)
        while True:
            broadcast = Broadcast.objects.filter(id=broadcast_id).last()
            need_to_remove = []
            if len(sorted_clients):
                for client in sorted_clients:
                    local_now = datetime.now(client.time_zone).replace(tzinfo=None)
                    start_time = broadcast.start_time.replace(tzinfo=None)
                    end_time = broadcast.end_time.replace(tzinfo=None)

                    if start_time <= local_now <= end_time:
                        print(local_now.replace(tzinfo=None))
                        print(f'do req {client.id}')
                        print(f'del client {client.id}')
                        message = Message.objects.create(
                            send_time=datetime.now(client.time_zone),
                            broadcast=broadcast,
                            client=client
                        )
                        logger.info(f'Message with id: {message.id} was created with data '
                                    f'send_time: {message.send_time} ;'
                                    f'broadcast_id: {broadcast.id} ;'
                                    f'client_id: {client.id} ;')
                        status_code = send_message(message.id, broadcast.text, client.phone_number)

                        message.status_code = status_code
                        message.save()
                        need_to_remove.append(client)
                    elif local_now >= end_time:
                        print(f'del client {client.id} by end time')
                        need_to_remove.append(client)
                    else:
                        print(f'wait client {client.id}')

                for client in need_to_remove:
                    sorted_clients.remove(client)
            else:
                break
            time.sleep(10)
