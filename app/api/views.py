import logging

from django_rest_api_logger import APILoggingMixin
from rest_framework import generics, status

# Create your views here.
from rest_framework.exceptions import NotFound
from rest_framework.response import Response
from rest_framework.views import APIView

from api.serializers import ClientSerializer, BroadcastSerializer
from api.utils import get_all_clients, get_broadcast_statistic, get_broadcast_statistic_by_pk, get_all_broadcasts
from api.tasks import start_broadcast


class ClientCreate(generics.CreateAPIView):
    serializer_class = ClientSerializer


class ClientUpdate(generics.RetrieveUpdateAPIView):
    serializer_class = ClientSerializer
    queryset = get_all_clients()


class ClientDestroy(generics.RetrieveDestroyAPIView):
    serializer_class = ClientSerializer
    queryset = get_all_clients()


class BroadcastCreate(generics.CreateAPIView):
    serializer_class = BroadcastSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)

        start_broadcast.delay(
            serializer.data.get('id')
        )

        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class BroadcastUpdate(generics.RetrieveUpdateAPIView):
    serializer_class = BroadcastSerializer
    queryset = get_all_broadcasts()


class BroadcastDestroy(generics.RetrieveDestroyAPIView):
    serializer_class = BroadcastSerializer
    queryset = get_all_broadcasts()


class BroadcastStatistic(APIView):

    def get(self, request, pk=None):
        if pk:
            data = get_broadcast_statistic_by_pk(pk)
            if data:
                return Response(data)
            else:
                raise NotFound(detail="Error 404, broadcast not found", code=404)
        else:
            return Response(get_broadcast_statistic())
