import logging

from django.db import models
from django.utils import timezone
from django.core.validators import RegexValidator
from timezone_field import TimeZoneField

logger = logging.getLogger(__name__)

class Broadcast(models.Model):
    class Meta:
        verbose_name = 'Рассылки'
        verbose_name_plural = 'Рассылка'

    start_time = models.DateTimeField(
        default=timezone.now,
        verbose_name='Время запуска рассылки'
    )

    # после этого времени рассылка должна остановиться!
    end_time = models.DateTimeField(
        null=True,
        blank=True,
        verbose_name='Время окончания рассылки'
    )

    text = models.TextField(
        verbose_name='Текст рассылки'
    )
    filter = models.TextField(
        blank=True,
        null=True
    )

    def delete(self, *args, **kwargs):
        logger.info(f'Broadcast with id: {self.id} was deleted!')
        return super().delete(*args, **kwargs)


class Client(models.Model):
    class Meta:
        verbose_name = 'Клиенты'
        verbose_name_plural = 'Клиент'

    phone_validator = RegexValidator(
        regex=r'^\+?7?\d{10}$',
        message="Номер телефона должен быть в формате: '7XXXXXXXXXX' (X - цифра от 0 до 9)"
    )

    phone_number = models.CharField(
        validators=[phone_validator],
        max_length=11,
        verbose_name='Номер телефона'
    )
    mobile_operator_code = models.CharField(
        max_length=3,
        verbose_name='Код мобильного оператора'
    )
    tag = models.CharField(
        max_length=100,
        null=True,
        blank=True,
        verbose_name='Произвольный тэг'
    )
    time_zone = TimeZoneField(
        choices_display="WITH_GMT_OFFSET",
        verbose_name='Часовой пояс',
        use_pytz=True
    )

    def delete(self, *args, **kwargs):
        logger.info(f'Client with id: {self.id} was deleted!')
        return super().delete(*args, **kwargs)


class Message(models.Model):
    class Meta:
        verbose_name = 'Сообщения'
        verbose_name_plural = 'Сообщение'

    send_time = models.DateTimeField(
        default=timezone.now,
        verbose_name='Время отправки',
    )

    status_code = models.IntegerField(
        verbose_name='Статус отправки',
        null=True, blank=True
    )

    broadcast = models.ForeignKey(
        to=Broadcast,
        on_delete=models.CASCADE,
        verbose_name='Объект рассылки'
    )
    client = models.ForeignKey(
        to=Client,
        on_delete=models.CASCADE,
        verbose_name='Объект клиента'
    )

    def delete(self, *args, **kwargs):
        logger.info(f'Message with id: {self.id} was deleted!')
        return super().delete(*args, **kwargs)


