from django.contrib import admin
from django.urls import path

from api.views import ClientCreate, ClientUpdate, ClientDestroy, BroadcastCreate, BroadcastStatistic, BroadcastUpdate, \
    BroadcastDestroy

urlpatterns = [

    # Client
    path('client/create/', ClientCreate.as_view(), name='client-create'),
    path('client/update/<int:pk>/', ClientUpdate.as_view(), name='client-update'),
    # Можно заменить на UpdateDestroyView
    path('client/destroy/<int:pk>/', ClientDestroy.as_view(), name='client-destroy'),
    # Можно заменить на UpdateDestroyView

    # Broadcasts

    path('broadcast/create/', BroadcastCreate.as_view(), name='broadcast-create'),
    path('broadcast/update/<int:pk>/', BroadcastUpdate.as_view(), name='broadcast-update'),
    # Можно заменить на UpdateDestroyView
    path('broadcast/destroy/<int:pk>/', BroadcastDestroy.as_view(), name='broadcast-destroy'),
    # Можно заменить на UpdateDestroyView

    path('broadcast/statistic/', BroadcastStatistic.as_view(), name='broadcast-statistic'),
    path('broadcast/statistic/<int:pk>/', BroadcastStatistic.as_view(), name='broadcast-statistic-detail'),
]
